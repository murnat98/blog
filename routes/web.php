<?php

use App\Http\Controllers\MainController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'Main'])->name('main');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::resource('posts', PostController::class)->names([
    'index' => 'posts-index',
    'create' => 'posts-create',
    'store' => 'posts-store',
    'show' => 'posts-show',
    'edit' => 'posts-edit',
    'update' => 'posts-update',
    'destroy' => 'posts-destroy'
]);
Route::get('my_posts', [PostController::class, 'myPostsList'])->name('my-posts-list');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
