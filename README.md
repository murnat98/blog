## Run

    git clone https://gitlab.com/murnat98/blog
    cd blog
    Rename .env.example .env, configuring database parameters and setting app_key
    composer install
    php artisan migrate
    php artisan serve
