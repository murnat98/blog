@section('title', 'All posts')

<x-guest-layout>
    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-dropdown')

        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <div class="flex justify-center">
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full w-32 mt-6">
                            <a href="{{ route('posts-create') }}">New post</a>
                        </button>
                    </div>
                    <h1 class="text-6xl ml-4">Posts</h1>
                    @foreach($posts as $post)
                        {{--<p><a href="{{ route('posts-show', ['post' => $post->id]) }}">{{ $post->header }}</a></p>--}}
                        <div class="ml-4 mb-6">
                            <p class="text-4xl">{{ $post->header }}</p>
                            <p class="text-base">
                                {{ $post->description }}
                                <a class="text-blue-400" href="{{ route('posts-show', ['post' => $post]) }}">More...</a>
                            </p>
                        </div>
                    @endforeach

                    <div class="flex justify-center mb-5">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
