@section('title', 'New post')

<x-guest-layout>
    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-dropdown')

        @include('forms/form', ['action' => route('posts-store'), 'method' => 'post'])
    </div>
</x-guest-layout>
