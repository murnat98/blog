@section('title', 'Post ' . $post->header)

<x-guest-layout>
    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-dropdown')

        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                            <div class="m-6">
                                @if($post->is_deleted)
                            <p class="text-red-700 text-4xl">DELETED!</p>
                        @endif
                        <h1 class="text-6xl">{{ $post->header }}</h1>
                        <p class="text-sm">posted by {{ $post->user()->getResults()['name'] }}
                            on {{ $post->created_at }}</p>
                        <p class="text-lg whitespace-pre">{{ $post->description }}</p>

                        @if (Auth::check() && Auth::user()->id == $post->user()->getResults()['id'])
                            <div>
                                <button
                                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full w-32"
                                >
                                    <a href="{{ route('posts-edit', ['post' => $post]) }}">Update</a>
                                </button>
                            </div>
                            @if(!$post->is_deleted)
                                <div class="mt-4">
                                    <button
                                            class="post-delete bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full w-32"
                                            data-url="{{ route('posts-destroy', ['post' => $post]) }}"
                                            data-post="{{ $post }}"
                                    >
                                        Delete
                                    </button>
                                </div>
                            @endif

                            <script>
                                $('.post-delete').on('click', function () {
                                    var data = JSON.parse($(this).attr('data-post'));
                                    data._token = $('meta[name=csrf-token]').attr('content');
                                    console.log(data);
                                    $.ajax($(this).attr('data-url'), {
                                        method: 'delete',
                                        data: data
                                    }).done(function () {
                                        $('.post-deleted').removeClass('invisible');
                                    }).fail(function () {
                                        $('.post-delete-error').removeClass('invisible');
                                    });
                                });
                            </script>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="text-white px-6 py-4 border-0 rounded fixed top-0 mt-6 ml-6 bg-green-500 closable post-deleted invisible">
            <span class="inline-block align-middle mr-8">
                <b class="capitalize">Deleted!</b> This post was successfully deleted!
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
                <span>×</span>
            </button>
        </div>
        <div class="text-white px-6 py-4 border-0 rounded fixed top-0 mt-6 ml-6 bg-red-500 closable post-delete-error invisible">
            <span class="inline-block align-middle mr-8">
                <b class="capitalize">Error!</b> Deletion error, please, reload the browser, and try again!
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none">
                <span>×</span>
            </button>
        </div>

        <script>
            $('.closable button').on('click', function() {
                $(this).parent().addClass('invisible');
            });
        </script>
    </div>
</x-guest-layout>
