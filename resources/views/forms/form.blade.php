<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <div class="m-6">
                <h1 class="text-5xl m-6">Create post</h1>

                <form class="w-full max-w-sm form" method="post" action="{{ $action }}">
                    @csrf
                    @method($method)

                    @foreach($columns as $column)
                        {!! $column->generateInputField() !!}
                    @endforeach

                    <div class="md:flex md:items-center">
                        <div class="md:w-1/3"></div>
                        <div class="md:w-2/3">
                            <button class="submit shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded"
                                    type="button">
                                Create post
                            </button>
                        </div>
                    </div>
                </form>

                <script>
                    $('.submit').on('click', function() {
                        $('.form').submit();
                    });
                </script>
            </div>
        </div>
    </div>
</div>
