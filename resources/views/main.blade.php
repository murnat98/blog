@section('title', 'Home')

<x-guest-layout>
    <div class="min-h-screen bg-gray-100">
        @livewire('navigation-dropdown')

        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <x-jet-welcome />
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
