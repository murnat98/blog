<!DOCTYPE html>
<html>
<head>
    <title>Blog - @yield('title')</title>
        <script src="{{ asset('js/jquery-3.5.1.min.js') }}?v0.1.0"></script>
</head>
<body>
    @section('menu')
        <h1>Menu</h1>
        <a href="{{ route('main') }}">Main</a>
        <a href="{{ route('posts-index') }}">All posts</a>
    @show

    @section('container')
        <h2>Container</h2>
    @show

    @section('footer')
        <h3>Footer</h3>
    @show
</body>
</html>