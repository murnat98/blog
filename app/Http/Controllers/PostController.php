<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    const PAGINATION = 10;

    public function __construct()
    {
        $this->middleware('auth')->only('myPostsList', 'create', 'store', 'edit', 'update', 'destroy');
    }

    /**
     * Display a listing of the posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('is_deleted', false)->paginate(self::PAGINATION);

        return view('posts/index', ['posts' => $posts]);
    }

    /**
     * Display all posts of current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function myPostsList()
    {
        $posts = Post::where('user_id', Auth::user()->id)->paginate(self::PAGINATION);

        return view('posts/index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post    = new Post;
        $columns = $post->getColumns();

        return view('posts/create', ['columns' => $columns]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'header' => 'required',
            'description' => 'required'
        ]);

        $fields = array();
        $validation = array();
        foreach (Post::getFields() as $field => $verboseName)
        {
            $fields[$field] = $request->post($field);
        }

        $fields['user_id'] = Auth::user()->id;

        $post = new Post($fields);
        $post->save();

        return redirect()->route('posts-show', ['post' => $post->getAttribute('id')]);
    }

    /**
     * Display the specified resource.
     * If post is deleted, and the user does not own the post, return 404 error.
     *
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if ($post->is_deleted)
        {
            if (!Auth::check() || $post->user()->getResults()['id'] != Auth::user()->id)
            {
                return abort(404);
            }
        }

        return view('posts/show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $postModel = new Post;
        $columns   = $postModel->getColumns($post);

        return view('posts/edit', ['columns' => $columns, 'post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'header' => 'required',
            'description' => 'required'
        ]);

        $currentUserId = Auth::user()->id;
        $postUserId    = $post->user()->getResults()['id'];
        $postId        = $post->getAttribute('id');

        if ($currentUserId != $postUserId)
        {
            Log::warning("User #{$currentUserId} trying to update post #{$postId}, which owns user #{$postUserId}");
            return redirect()->route('main');
        }

        foreach (Post::getFields() as $field => $verboseName)
        {
            $post->setAttribute($field, $request->post($field));
        }

        $post->save();

        return redirect()->route('posts-show', ['post' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->setAttribute('is_deleted', true);
        $post->save();

        return response(['status' => 'ok']);
    }
}
