<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * Class Column
 *
 * @package App\Models
 */
class Column
{
    protected $type;
    protected $verboseName;
    protected $name;
    protected $defaultValue;

    public function __construct(string $type, string $verboseName, string $name, string $defaultValue = "")
    {
        $this->type = $type;
        $this->verboseName = $verboseName;
        $this->name = $name;
        $this->defaultValue = $defaultValue;
    }

    /**
     * Generate input field to show in the HTML.
     * TODO: now realized only string and text types. Realize all field types.
     *
     * @return string
     */
    public function generateInputField()
    {
        $viewName = '';
        switch($this->type)
        {
            case 'string':
                $viewName = 'forms/text-input';
                break;
            case 'text':
                $viewName = 'forms/string-input';
                break;
        }

        $input = view($viewName, [
            'label' => $this->verboseName,
            'name' => $this->name,
            'initialValue' => $this->defaultValue
        ]);

        return $input;
    }
}

/**
 * Trait HasColumns, which has can get Column class for fields, described in model.
 * `getFields` method should return an array with fields with the structure, shown below:
 * [ 'verbose_name' => 'name_in_database', ... ]
 *
 * @package App\Models
 */
trait HasColumns
{
    /**
     * Get form columns (for creation or update).
     *
     * @param Model|null $model
     * @return array
     */
    function getColumns(Model $model = null) {
        if (!method_exists(self::class, 'getFields'))
        {
            Log::error('`getFields` method not provided in model class while using HasColumns trait');
            return array();
        }

        $columnListing = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
        $columns = array();

        foreach ($columnListing as $column)
        {
            if (array_key_exists($column, self::getFields()))
            {
                $columnType = $this->getConnection()->getDoctrineColumn($this->getTable(), $column)->getType()->getName();
                $defaultValue = is_null($model) ? "" : $model->getAttribute($column);
                array_push($columns, new Column($columnType, self::getFields()[$column], $column, $defaultValue));
            }
        }

        return $columns;
    }
}