<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory, HasColumns;

    protected static $fields = [
        'header' => 'Header',
        'description' => 'Description'
    ];

    public static function getFields()
    {
        return self::$fields;
    }

    public function __construct(array $attributes = [])
    {
        $this->fillable = array_keys(self::getFields());
        array_push($this->fillable, 'user_id');
        parent::__construct($attributes);
    }

    /**
     * Post creator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
